package com.ecommerce.ecommerce;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ecommerce.dto.ProductResponseDto;
import com.ecommerce.dto.ReviewDto;
import com.ecommerce.entity.OrderDetail;
import com.ecommerce.entity.Product;
import com.ecommerce.entity.UserDetail;
import com.ecommerce.exceptions.ProductsNotFoundException;
import com.ecommerce.repository.OrderDetailRepository;
import com.ecommerce.repository.ProductRepository;
import com.ecommerce.repository.UserDetailRepository;
import com.ecommerce.serviceimpl.ProductBuyServiceImpl;
import com.ecommerce.serviceimpl.ProductByRatingImpl;
import com.ecommerce.serviceimpl.ProductReviewServiceImpl;

@ExtendWith(SpringExtension.class)
class EcommerceApplicationTests {

	Product productone;
	
	Product producttwo;
	UserDetail userDetailone;
	OrderDetail orderDetail;
	ReviewDto reviewDto;
	List<Product> listofproducts;
	ProductResponseDto productResponseDto;
	@Mock
	UserDetailRepository userDetailRepository;
    @Mock
	OrderDetailRepository orderDetailRepository;
    @Mock
    ProductRepository productRepository;
    @InjectMocks
    ProductReviewServiceImpl productReviewService;
    @InjectMocks 
    ProductBuyServiceImpl productBuyServiceImpl;
    @InjectMocks
    ProductByRatingImpl productByRatingImpl;
	
	@BeforeEach
	public void setup()
	{
		orderDetail =new OrderDetail();
		userDetailone=new UserDetail();
		userDetailone.setEmail("hanumantha@gmail.com");
		userDetailone.setPhonenumber("997868675");
		userDetailone.setUserid(1);
		userDetailone.setUsername("hanuman");
		orderDetail.setOrderid(1);
		orderDetail.setProductid(1);
		orderDetail.setRating(3);
		orderDetail.setReview("just ok ok");
		productone=new Product();
		productone.setCost(2000);
		productone.setProductid(1);
		productone.setProductname("Dell");
		productone.setQundity(89);
		productone.setRating(4);
		
		
	}
	@Test
	public void addreviews()
	{
		int userid=1;
		int productid=1;
		reviewDto=new ReviewDto();
		orderDetail.setUserDetail(userDetailone);
		Mockito.when(userDetailRepository.findByUserid(Mockito.anyInt())).thenReturn(userDetailone);
		Mockito.when(orderDetailRepository.save(orderDetail)).thenReturn(orderDetail);
	
		assertEquals(HttpStatus.OK.value(), productReviewService.providereviewtoproduct(userid, productid).getBody().getStatuscode());
	}
	
	@Test
	public void productbuyNegitive()
	{
		int productid=1;
		producttwo=new Product();
		Mockito.when(productRepository.findByProductid(Mockito.anyInt())).thenReturn(Optional.of(producttwo));
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),productBuyServiceImpl.productbuybyid(productid).getBody().getStatuscode());
	}
	@Test
	public void productbuyPositive()
	{
		int productid=1;
	
		Mockito.when(productRepository.findByProductid(Mockito.anyInt())).thenReturn(Optional.of(productone));
		Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(productone);
		assertEquals(HttpStatus.OK.value(),productBuyServiceImpl.productbuybyid(productid).getBody().getStatuscode());
	}
	  @Test
	    public void getProductspositive() throws ProductsNotFoundException
	    {
		  String catalogname="laptop";
		  listofproducts=new ArrayList<>();
		  listofproducts.add(productone);
	        Mockito.when(productRepository.findAllByCategory_categorynameOrderByRatingDesc(Mockito.anyString())).thenReturn(listofproducts);
	       
	       assertEquals(1, productByRatingImpl.getallproducts(catalogname).size());
	        
	    }
	  @Test
	    public void getProductsnegitive() throws ProductsNotFoundException
	    {
		  String catalogname="laptop";
		  listofproducts=new ArrayList<>();
		
	        Mockito.when(productRepository.findAllByCategory_categorynameOrderByRatingDesc(Mockito.anyString())).thenReturn(listofproducts);
	       
	      assertThrows(ProductsNotFoundException.class, ()->productByRatingImpl.getallproducts(catalogname));
	        
	    }
}

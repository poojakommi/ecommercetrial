package com.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.entity.UserDetail;

public interface UserDetailRepository extends JpaRepository<UserDetail,Integer>{

	UserDetail findByUserid(int userid);
	

}

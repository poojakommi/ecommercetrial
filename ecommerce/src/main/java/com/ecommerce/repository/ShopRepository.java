package com.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.entity.Shop;

public interface ShopRepository extends JpaRepository<Shop, Integer> {

}

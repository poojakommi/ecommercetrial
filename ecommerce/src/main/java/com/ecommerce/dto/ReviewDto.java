package com.ecommerce.dto;

public class ReviewDto {
	
	private int statuscode;
	private String message;
	public int getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ReviewDto(int statuscode, String message) {
		super();
		this.statuscode = statuscode;
		this.message = message;
	}
	public ReviewDto() {
	//create object
	}
	

}

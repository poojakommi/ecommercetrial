package com.ecommerce.exceptions;

public class ProductsNotFoundException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public ProductsNotFoundException() {
		super();
	}

	public ProductsNotFoundException(final String message) {
		super(message);
	}

}

package com.ecommerce.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UserDetail {
	@Id
	@GeneratedValue
	private int userid;
	private String username;
	private String email;
	private String phonenumber;
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public UserDetail(int userid, String username, String email, String phonenumber) {
		super();
		this.userid = userid;
		this.username = username;
		this.email = email;
		this.phonenumber = phonenumber;
	}
	public UserDetail() {
//create object
	}
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "userDetail")
    private Set<OrderDetail> order;
	public Set<OrderDetail> getOrder() {
		return order;
	}
	public void setOrder(Set<OrderDetail> order) {
		this.order = order;
	}
	public UserDetail(Set<OrderDetail> order) {
		super();
		this.order = order;
	}
	
}

package com.ecommerce.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
@Entity
public class Shop {
	
	@Id
	@GeneratedValue
	private int shopid;
	private String shopname;
	@Column(nullable = false, unique = true)
	private String gstnumber;
	private int rating;
	public int getShopid() {
		return shopid;
	}
	public void setShopid(int shopid) {
		this.shopid = shopid;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public String getGstnumber() {
		return gstnumber;
	}
	public void setGstnumber(String gstnumber) {
		this.gstnumber = gstnumber;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Shop(int shopid, String shopname, String gstnumber, int rating) {
		super();
		this.shopid = shopid;
		this.shopname = shopname;
		this.gstnumber = gstnumber;
		this.rating = rating;
	}
	public Shop() {
//create object
	}
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "shop")
	Set<Category> category;
	public Set<Category> getCategory() {
		return category;
	}
	public void setCategory(Set<Category> category) {
		this.category = category;
	}
	public Shop(Set<Category> category) {
		super();
		this.category = category;
	}

}

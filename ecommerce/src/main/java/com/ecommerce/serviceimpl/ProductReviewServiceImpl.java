package com.ecommerce.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ecommerce.dto.ReviewDto;
import com.ecommerce.entity.OrderDetail;
import com.ecommerce.entity.UserDetail;
import com.ecommerce.repository.OrderDetailRepository;
import com.ecommerce.repository.UserDetailRepository;
import com.ecommerce.service.ProductReviewService;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {

	@Autowired
	UserDetailRepository userDetailRepository;
	@Autowired
	OrderDetailRepository orderDetailRepository;

	public ResponseEntity<ReviewDto> providereviewtoproduct(int userid, int productid) {
		UserDetail userdetails = userDetailRepository.findByUserid(userid);
		ReviewDto reviewdto = new ReviewDto();
		OrderDetail orderdetails = new OrderDetail();
		orderdetails.setProductid(productid);
		orderdetails.setRating(4);
		orderdetails.setReview("excellent product");
		orderdetails.setUserDetail(userdetails);

		orderDetailRepository.save(orderdetails);
		reviewdto.setMessage("Review added");
		reviewdto.setStatuscode(HttpStatus.OK.value());
		return new ResponseEntity<>(reviewdto, HttpStatus.OK);

	}

}

package com.ecommerce.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ecommerce.dto.ProductResponseDto;
import com.ecommerce.entity.Product;
import com.ecommerce.repository.ProductRepository;
import com.ecommerce.service.ProductBuyService;
@Service
public class ProductBuyServiceImpl implements ProductBuyService{
	
	@Autowired
	ProductRepository productRepository;

	public ResponseEntity<ProductResponseDto>  productbuybyid(int productid)
	{
		ProductResponseDto productResponseDto=new ProductResponseDto();
		Optional<Product> product=productRepository.findByProductid(productid);
		if(product.isPresent()&&(product.get().getQundity()>=1))
		{
			product.get().setQundity(product.get().getQundity()-1);
			productRepository.save(product.get());
			productResponseDto.setStatuscode(HttpStatus.OK.value());
			productResponseDto.setMessgae("Successfully Bought the product");
			return new ResponseEntity<>(productResponseDto,HttpStatus.OK);
			}
		else
		{
			productResponseDto.setStatuscode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			productResponseDto.setMessgae("product is not avaiable");
			
			return new ResponseEntity<>(productResponseDto,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.ecommerce.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.entity.Product;
import com.ecommerce.exceptions.ProductsNotFoundException;
@Service
public interface ProductsByRating {
	
	public  List<Product> getallproducts(String catelogname) throws ProductsNotFoundException;

}

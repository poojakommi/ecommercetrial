package com.ecommerce.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ecommerce.dto.ReviewDto;
@Service
public interface ProductReviewService {
	public ResponseEntity<ReviewDto> providereviewtoproduct(int userid,int productid);
}

package com.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.dto.ProductResponseDto;
import com.ecommerce.dto.ReviewDto;
import com.ecommerce.entity.Product;
import com.ecommerce.exceptions.ProductsNotFoundException;
import com.ecommerce.service.ProductBuyService;
import com.ecommerce.service.ProductReviewService;
import com.ecommerce.service.ProductsByRating;

@RestController
@RequestMapping("ecommerce")
public class EcommereceController {
	
	@Autowired
	ProductsByRating productsByRating;
	
	@Autowired
	ProductBuyService productBuyService;
	
	@Autowired
	ProductReviewService productReviewService;
	@GetMapping("productsbycategory/{catalogname}")
	public List<Product> getallproductsbyrating(@PathVariable String catalogname) throws ProductsNotFoundException
	{
		return productsByRating.getallproducts(catalogname);
	}
	@PostMapping("products/{productid}")
	public ResponseEntity<ProductResponseDto> buyproductbyid(@PathVariable int productid)
	{
		return productBuyService.productbuybyid(productid);
	}
	@PostMapping("reviews")
	public ResponseEntity<ReviewDto> providereviews(@RequestParam(name="userid") int userid,@RequestParam(name="productid") int productid)
	{
		return productReviewService.providereviewtoproduct(userid, productid);
	}

}
